import model.Author;
import model.Book;
import model.BookType;
import model.Human;
import service.HibernateService;
import service.MySQLService;
import table.AuthorManager;
import table.BookManager;

import java.util.List;
import java.util.concurrent.Executor;

public class Main {

    private static HibernateService hibernateService;

    public static void main(String[] args) {
        hibernateService = new MySQLService();
        hibernateService.connect("hibernate.cfg.xml");
        AuthorManager authorManager = new AuthorManager(hibernateService);

        Human humanPawel = new Human("Paweł", "Maksymiuk", "1981");
        Author authorPawel = new Author(humanPawel);
        authorManager.add(authorPawel);

  /*      Human human = new Human();
        human.setName("Henryk");
        human.setLastName("Chmielewski");
        human.setYearOfBirth("1923");
        Author author = new Author();
        author.setHuman(human);
//        author.setAuthorId("1"); - niepotrzebne, bo generuje się automatycznie
        authorManager.add(author);
/*
        Human human1 = new Human();
        human1.setName("Henryk");
        human1.setLastName("Sienkiewicz");
        human1.setYearOfBirth("1846");
        Author author1 = new Author();
        author.setHuman(human1);
        authorManager.add(author1);

        Human human2 = new Human();
        human2.setName("Wisława");
        human2.setLastName("Szymborska");
        human2.setYearOfBirth("1923");
        Author author2 = new Author();
        author.setHuman(human2);
        authorManager.add(author2);
*/
        BookManager bookManager = new BookManager(hibernateService);
  /*
        Book book1 = new Book();
        book1.setName("Lokomotywa");
        book1.setBookType(BookType.POEM);
        //book1.setId("1"); - niepotrzebne, bo generuje się automatycznie
        bookManager.add(book1);

        Book book2 = new Book();
        book2.setName("Potop");
        book2.setBookType(BookType.PROSE);
        bookManager.add(book2);
*/
        /*Book book3 = new Book();
        book3.setName("Zniewolony umysł");
        book3.setBookType(BookType.PROSE);
        bookManager.add(book3);*/
/*
        List<Author> authors = authorManager.getList();
        for (Author author3 : authors) {
            System.out.println(author3);
        }*/

        /*Author authorUpdate = new Author();
        authorUpdate.setId("2c90c6cf635419f301635419f5f70000");
        Human humanUpdate = new Human();
        humanUpdate.setYearOfBirth("1911");
        humanUpdate.setName("Czesław");
        humanUpdate.setLastName("Miłosz");
        authorUpdate.setHuman(humanUpdate);
        authorManager.update("2c90c6cf635419f301635419f5f70000", authorUpdate);

        authorManager.delete("2c90c6cf635419f301635419f6170001");*/

        hibernateService.disconnect();


//        Executor executor = new DBExecutor(hibernateService);
//        DataManager<Author> authorDataManager = new AuthorsManager(executor);
//        DataManager<Book> bookDataManager = new BooksManager(executor);
//        Author authorP = new Author();


    }

}
