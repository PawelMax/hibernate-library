package table;

import model.Author;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import service.HibernateService;

import javax.persistence.Embedded;
import java.util.ArrayList;
import java.util.List;

public class AuthorManager {

    private HibernateService service;

    public AuthorManager(HibernateService service){
        this.service = service;
    }

    public void add(Author author){
        service.openSession();
        Session session = service.getSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.save(author);
            transaction.commit(); //TODO: co tu się dzieje?
        }catch (HibernateException e){
            if (transaction!= null) transaction.rollback();
            e.printStackTrace();
        }finally {
            service.closeSession();
        }
    }
    public List<Author> getList(){
        List<Author> authors = new ArrayList<>();
        service.openSession();
        Session session = service.getSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            authors = session.createQuery("FROM Author").list();
            transaction.commit();
        } catch (HibernateException e) {

        }finally {
            session.close();
        }
        return authors;
    }

    public void update (String id, Author author){
        service.openSession();;
        Session session = service.getSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            //author = session.get(Author.class, id);
            session.update(author);
            transaction.commit();
        } catch (HibernateException e){
            if (transaction != null) transaction.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
    }

    public void delete(String id){
        service.openSession();;
        Session session = service.getSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Author author = session.get(Author.class, id);
            session.delete(author);
            transaction.commit();
        }catch (HibernateException e){
            if(transaction != null) transaction.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
    }
}
