package table;


import model.Book;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import service.HibernateService;

public class BookManager {

    private HibernateService service;

    public BookManager(HibernateService service) {
        this.service = service;
    }

    public void add(Book book){
        service.openSession();
        Session session = service.getSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.save(book);
            transaction.commit();
        }catch (HibernateException e){
            if (transaction != null) transaction.rollback();
            e.printStackTrace();
        }finally {
            service.closeSession();
        }
    }
}
