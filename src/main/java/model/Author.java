package model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Table(name = "authors")
@AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "AUTHOR_ID"))})
public class Author extends BaseModel{

    public static final String TABLE_NAME = "Author";

    @Embedded
    private Human human;

    @ManyToMany
    private Collection<Book> books = new ArrayList<>();

    public Author() {
    }

    public Author(Human human) {
        this.human = human;
    }

    public Human getHuman() {
        return human;
    }

    public void setHuman(Human human) {
        this.human = human;
    }

    public String getAuthorId() {
        return id;
    }

    public void setAuthorId(String authorId) {
        this.id = authorId;
    }

    public void setId(String id){
        this.id = id;
    }

    /*public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getYearOfBirth() {
        return yearOfBirth;
    }

    public void setYearOfBirth(int yearOfBirth) {
        this.yearOfBirth = yearOfBirth;
    }
*/    @Override
    public String toString() {
        return "Author{" +
                "firstName: '" + human.getName() + '\'' +
                ", lastName: '" + human.getLastName() + '\'' +
                ", yearOfBirth: " + human.getYearOfBirth() +
                ", authorID: '" + id + '\'' +
                '}';
    }
}
