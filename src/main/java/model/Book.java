package model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Table(name = "books")
public class Book extends BaseModel {

    public static final String TABLE_NAME = "Book";

    @Column(name = "BOOK_NAME")
    private String name;

    @ManyToMany
    @JoinColumn(name = "id")
    private Collection<Author> authors = new ArrayList<>();

    @Column(name = "BOOK_TYPE")
    private BookType bookType;

    public Book(String name, Collection<Author> authors, BookType bookType) {
        this.name = name;
        this.authors = authors;
        this.bookType = bookType;
    }

    public Book() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

/*    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author authorId) {
        this.author = authorId;
    }*/

    @Enumerated(EnumType.STRING)
    public BookType getBookType() {
        return bookType;
    }

    public void setBookType(BookType bookType) {
        this.bookType = bookType;
    }

    @Override
    public String toString() {
        return "Book{" +
                "bookID: '" + id + '\'' +
                ", name: '" + name + '\'' +
                ", bookType: " + bookType +
                '}';
    }

}
